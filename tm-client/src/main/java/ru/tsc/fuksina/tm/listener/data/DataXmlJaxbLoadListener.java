package ru.tsc.fuksina.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.DataXmlJaxbLoadRequest;
import ru.tsc.fuksina.tm.dto.request.UserLogoutRequest;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.event.ConsoleEvent;

@Component
public final class DataXmlJaxbLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlJaxbLoadListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LOAD DATA FROM XML FILE]");
        getDomainEndpoint().loadDataXmlJaxb(new DataXmlJaxbLoadRequest(getToken()));
        getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

}
