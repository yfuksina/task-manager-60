package ru.tsc.fuksina.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.fuksina.tm.api.repository.dto.IUserOwnedRepositoryDto;
import ru.tsc.fuksina.tm.api.service.dto.IServiceDto;
import ru.tsc.fuksina.tm.api.service.dto.IUserOwnedServiceDto;
import ru.tsc.fuksina.tm.dto.model.AbstractUserOwnedModelDto;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.exception.field.IdEmptyException;
import ru.tsc.fuksina.tm.exception.field.UserIdEmptyException;

import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractUserOwnedServiceDto<M extends AbstractUserOwnedModelDto, R extends IUserOwnedRepositoryDto<M>> extends AbstractServiceDto<M, R> implements IUserOwnedServiceDto<M>, IServiceDto<M> {

    @NotNull
    protected abstract IUserOwnedRepositoryDto<M> getRepository();

    @Override
    @Transactional
    public void add(@Nullable final String userId, @NotNull M model) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        repository.add(userId, model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (sort == null) return findAll(userId);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        return repository.findAll(userId, sort);
    }

    @Nullable
    @Override
    public  M findOneById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        return repository.findOneById(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @NotNull final M model) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        repository.remove(userId, model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        repository.removeById(userId, id);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        repository.clear(userId);
    }

    @Override
    @Transactional
    public void update(@Nullable final String userId, @NotNull final M model) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        repository.update(userId, model);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        return repository.existsById(userId, id);
    }

    @Override
    public long getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        return repository.getSize(userId);
    }

}
