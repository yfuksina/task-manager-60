package ru.tsc.fuksina.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedRepositoryDto<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
