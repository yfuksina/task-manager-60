package ru.tsc.fuksina.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.fuksina.tm.api.repository.dto.IRepositoryDto;
import ru.tsc.fuksina.tm.api.service.dto.IServiceDto;
import ru.tsc.fuksina.tm.dto.model.AbstractModelDto;
import ru.tsc.fuksina.tm.exception.field.IdEmptyException;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractServiceDto<M extends AbstractModelDto, R extends IRepositoryDto<M>> implements IServiceDto<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract IRepositoryDto<M> getRepository();

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        @NotNull final IRepositoryDto<M> repository = getRepository();
        repository.add(model);
    }

    @Override
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        @NotNull final IRepositoryDto<M> repository = getRepository();
        repository.set(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IRepositoryDto<M> repository = getRepository();
        return repository.findAll();
    }

    @Nullable
    @Override
    public  M findOneById(@Nullable final String id) {
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final IRepositoryDto<M> repository = getRepository();
        return repository.findOneById(id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        @NotNull final IRepositoryDto<M> repository = getRepository();
        repository.remove(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final IRepositoryDto<M> repository = getRepository();
        repository.removeById(id);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final IRepositoryDto<M> repository = getRepository();
        repository.update(model);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final IRepositoryDto<M> repository = getRepository();
        repository.clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final IRepositoryDto<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Override
    public long getSize() {
        @NotNull final IRepositoryDto<M> repository = getRepository();
        return repository.getSize();
    }

}
