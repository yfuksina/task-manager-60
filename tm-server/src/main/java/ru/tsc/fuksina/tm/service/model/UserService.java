package ru.tsc.fuksina.tm.service.model;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.fuksina.tm.api.repository.model.ITaskRepository;
import ru.tsc.fuksina.tm.api.repository.model.IUserRepository;
import ru.tsc.fuksina.tm.api.service.IPropertyService;
import ru.tsc.fuksina.tm.api.service.model.IUserService;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.exception.system.AccessDeniedException;
import ru.tsc.fuksina.tm.model.User;
import ru.tsc.fuksina.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Optional;

@Getter
@Service
public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserRepository repository;

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository repository = getRepository();
        return repository.findOneByLogin(login);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findOneByLogin(login) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneByEmail(@Nullable final String email) {
        Optional.ofNullable(email).filter(item -> !item.isEmpty()).orElseThrow(EmailEmptyException::new);
        @NotNull final IUserRepository repository = getRepository();
        return repository.findOneByEmail(email);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findOneByEmail(email) != null;
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findOneByLogin(login);
        remove(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        @NotNull final IUserRepository repository = getRepository();
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).filter(item -> !item.isEmpty()).orElseThrow(EmailEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @Nullable final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        @NotNull final IUserRepository repository = getRepository();
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        if (isLoginExists(login)) throw new LoginExistsException();
        @Nullable final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final IUserRepository repository = getRepository();
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        @Nullable final User user = findOneById(userId);
        if (user == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        update(user);
        return user;
    }

    @Nullable
    @Override
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        @Nullable final User user = findOneById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @Nullable final User user= findOneByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        update(user);
    }

}
